module.exports = function(grunt) {
    'use strict';

//    var rename = function (dest, src) { return dest + src.substring(0, src.indexOf('.min')) + '.js';};
//    var renameVersion = function (dest, src) { return dest + src.substring(0, src.indexOf('-')) + '.js'; };

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        watch: {
            options: {
                livereload: true
            },
            assets: {
                files: [
                    'src/main/webapp/resources/**/*.js',
                    'src/main/webapp/resources/**/*.less',
                    'src/main/webapp/resources/**/*.html',
                    'src/main/webapp/WEB-INF/views/*.jsp'
                ],
                tasks: ['build', /*'jshint', 'karma:watch:run'*/]
            }
        },

        less: {
            dist: {
                files: [{
                    dest: "src/main/webapp/resources/css/build.css",
                    src: "src/main/webapp/resources/css/project.less"
                }]
            }
        },
        usemin: {
            html: ['src/main/webapp/WEB-INF/layout/default-layout.jsp','src/main/webapp/WEB-INF/layout/no-nav-layout.jsp','src/main/webapp/WEB-INF/layout/voucher-layout.jsp','src/main/webapp/WEB-INF/views/help.jsp','src/main/webapp/WEB-INF/views/errors/error.jsp','src/main/webapp/WEB-INF/views/errors/403.jsp'],
            options: {
              blockReplacements: {
                css: function (block) {
                    return '<link rel="stylesheet" type="text/css" href="<c:url value=\'' + block.dest + '\'/>"/>';
                }
              }
            }
        },
        clean: [/*'src/main/webapp/resources/scripts/build.js',*/ 'src/main/webapp/resources/css/build.css'],
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-clear');
    grunt.loadNpmTasks('grunt-usemin');

    grunt.registerTask('default', ['build']);
    grunt.registerTask('live', [/*'karma:watch',*/ 'watch:assets']);

    grunt.registerTask('build', ['clean', /*'jshint', 'gitinfo', 'replace', 'concat',*/ 'less', 'usemin']);

};