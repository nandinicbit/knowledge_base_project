package com.springapp.mvc;

import com.springapp.dao.docToPersonDao;
import com.springapp.dao.documentsDao;
import com.springapp.dao.personDao;
import com.springapp.data.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/")
public class HelloController {

    @Autowired
    private personDao personDaoInst;

    @Autowired
    private docToPersonDao docToPersonInst;

    @Autowired
    private documentsDao documentsDaoInst;

	@RequestMapping(method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		model.addAttribute("message", "Hello world!");
		return "knowledgeBase";

       /* ModelAndView mav = new ModelAndView();
        mav.setViewName("home");
        return mav;*/
	}

    @RequestMapping(method = RequestMethod.GET,value="/ohss.htm")
    public String ohss(ModelMap model) {
       return "/ohss/ohss";
    }

    @RequestMapping(method = RequestMethod.GET,value="/lhat.htm")

    public String lhat(ModelMap model) {
        return "lhat";
    }
    @RequestMapping(method = RequestMethod.GET,value="/sit.htm")

    public String sit(ModelMap model) {
        return "sit";
    }
    @RequestMapping(method = RequestMethod.GET,value="/bio.htm")

    public String bio(ModelMap model) {
        return "bio";
    }

    @RequestMapping(method = RequestMethod.GET,value="/coreServices.htm")
    public String coreServices(ModelMap model) {
        return "coreServices";
    }

    @RequestMapping(method = RequestMethod.GET,value="/ohss_faq.htm")
    public String ohssFaq(ModelMap model) {
        return "/ohss/ohss_faq";
    }

    @RequestMapping(method = RequestMethod.GET,value="/ohss_workflow.htm")
    public String ohssWorkflow(ModelMap model) {
        return "/ohss/ohss_workflow";
    }

    @RequestMapping(method = RequestMethod.GET,value="/ohss_ui_page_explanation.htm")
    public String ohssUiPageExplanation(ModelMap model) {
        return "/ohss/ohss_ui_page_explanation";
    }


    @RequestMapping(method = RequestMethod.GET,value="/ohss_database_explanation.htm")
    public String ohssDatabaseExplanation(ModelMap model) {
        return "/ohss/ohss_database_explanation";
    }

    @RequestMapping(method = RequestMethod.GET,value="/ohss_setup.htm")
    public String ohssSetup(ModelMap model) {
        return "/ohss/ohss_setup";
    }

    @RequestMapping(method = RequestMethod.GET,value="/ohss_controller_explanation.htm")
    public String ohssControllerExplanation(ModelMap model) {
        return "/ohss/ohss_controller_explanation";
    }


    @RequestMapping(method = RequestMethod.GET,value="/ohss_dao_explanation.htm")
    public String ohssDaoExplanation(ModelMap model) {
        return "/ohss/ohss_dao_explanation";
    }


    @RequestMapping(method = RequestMethod.GET,value="/ohss_services_explanation.htm")
    public String ohssServicesExplanation(ModelMap model) {
        return "/ohss/ohss_services_explanation";
    }

    @RequestMapping(method = RequestMethod.GET,value="/angular.htm")
    public String angular(ModelMap model) {
        return "angular";
    }

    @RequestMapping(value="/People.htm", method = RequestMethod.GET)
    public ModelAndView getPeople(){
        return new ModelAndView("People");
    }

    @RequestMapping(value="/addPeople", method = RequestMethod.POST)
    public @ResponseBody String addPeople(@RequestBody Person firstName) {
        ModelAndView mav = new ModelAndView("People");
        Person person = new Person();
        person.setFirstName(firstName.getFirstName());
        person.setCity("");
        person.setAddress("");

        personDaoInst.addPerson(person);

        return "";
    }



}