package com.springapp.dao;


import com.springapp.data.Person;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;


@Service("personDao")
public class personDaoImpl  implements personDao {
	 private SessionFactory sessionFactory;
	 
	public void setSessionFactory(SessionFactory sessionFactory) {
	     this.sessionFactory = sessionFactory;
	 }
	
	
	@Override
	public Person getPerson(int id) {
		org.hibernate.Session session = sessionFactory.openSession();
		return (Person) session.createQuery(" from Person p where p.ID = :id").setParameter("id", id).uniqueResult();
		
	}
	
	@Override
	public void addPerson(Person person) {
		org.hibernate.Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
	 	Query sqlQuery = session.createSQLQuery("insert into Person(firstName,lastName,address,city) values(:firstName,:lastName,:address,:city)")
	 			.setParameter("firstName",person.getFirstName())
	 			.setParameter("lastName",person.getLastName())
	 			.setParameter("address",person.getAddress())
	 			.setParameter("city",person.getCity());
        int x = sqlQuery.executeUpdate();
		tx.commit();
		
	}



}
