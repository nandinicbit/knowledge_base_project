package com.springapp.dao;

import com.springapp.data.Person;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface personDao {
	
	public Person getPerson(int personId);

	public void addPerson(Person person);
	

	

}
