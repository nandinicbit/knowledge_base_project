package com.springapp.dao;

import com.springapp.data.docToPerson;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Service;


@Service("docToPersonDao")
public  class docToPersonDaoImpl implements docToPersonDao {
	 private SessionFactory sessionFactory;
	 
		public void setSessionFactory(SessionFactory sessionFactory) {
		     this.sessionFactory = sessionFactory;
		 }
		
	
	@Override
		public docToPerson getDoc() {
			org.hibernate.Session session = sessionFactory.openSession();
			return (docToPerson) session.createQuery("from docToPerson d where id = 2").uniqueResult();
			
					
		}
	
	
	@Override
	public void uploadDoc(docToPerson x) {
		org.hibernate.Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(x);
		session.getTransaction().commit();//Dont forget to commit transaction
		session.close();
				
	}
	

}
