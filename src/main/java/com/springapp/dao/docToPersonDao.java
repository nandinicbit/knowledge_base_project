package com.springapp.dao;

import com.springapp.data.docToPerson;


public interface docToPersonDao {
	public docToPerson getDoc();

	public void uploadDoc(docToPerson x);

}
