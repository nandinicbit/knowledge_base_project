package com.springapp.dao;

import com.springapp.data.Documents;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Service;


@Service("documentsDao")
public  class documentsDaoImpl implements documentsDao {
	 private SessionFactory sessionFactory;
	 
		public void setSessionFactory(SessionFactory sessionFactory) {
		     this.sessionFactory = sessionFactory;
		 }
		
	
	@Override
		public Documents getDoc() {
			org.hibernate.Session session = sessionFactory.openSession();
			return (Documents) session.createQuery("from Documents d").uniqueResult();
			
					
		}
	

}
