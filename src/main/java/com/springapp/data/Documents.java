package com.springapp.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;


@Entity
@Table(name="Documents")

public class Documents {

	public Documents() {
		// TODO Auto-generated constructor stub
	}

	public byte[] getDocument() {
		return document;
	}
	public void setDocument(byte[] document) {
		this.document = document;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Id
	@Column(name="Id")
	private UUID id;
	private String name;
	@Column(name = "document", columnDefinition = "VARBINARY")
	private byte[] document;

}