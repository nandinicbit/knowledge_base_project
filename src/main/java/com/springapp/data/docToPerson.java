package com.springapp.data;



import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="docToPerson")

public class docToPerson {

	public docToPerson() {
		// TODO Auto-generated constructor stub
	}

	public byte[] getDocument() {
		return document;
	}
	public void setDocument(byte[] document) {
		this.document = document;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public int getPerson() {
		return person;
	}

	public void setPerson(int person) {
		this.person = person;
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Id
	@Column(name="Id")
	private int id;
	private int person;
	private String name;
	@Column(name = "document", columnDefinition = "VARBINARY")
	private byte[] document;

}