package com.springapp.data;

import javax.persistence.*;
import java.io.Serializable;


@Entity(name="Person")

public class Person implements Serializable,Cloneable{
	private static final long serialVersionUID = 1L;
	// private static final long serialVersionUID = -1798070786993154676L;
	public Person() {
		// TODO Auto-generated constructor stub
	}
	
	public Integer getID() {
		return this.ID;
	} 
	

	public void setID(Integer id) {
		this.ID = id;
	}
	
	
	public String getFirstName() {
		return firstName;
	}
	
	

	public void setFirstName(String firstName) {
		this.firstName = firstName;
 	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	
	
	
	@Id
	@Basic(optional = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	private Integer ID;
	private String firstName;
	private String lastName;
	private String address;
	private String city;

}
