/**
 * Created by nparimi on 3/26/15.
 */

var app = angular.module('parimi',[]);


app.controller('myCtrl',function($scope,$http){
    $scope.firstName = 'Nandini';
    $scope.lastName = 'Parimi';
    $scope.message = '';
    $scope.charLeft = function(){return 200 - $scope.message.length;}
    $scope.save = function(){
        $http.post('/spring-demo/addPeople.htm',{"firstName": $scope.firstName,"lastName": $scope.lastName},{'Content-type': 'application/json'
        });
    }
});