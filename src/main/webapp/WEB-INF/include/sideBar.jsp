<div class="col-xs-2 sidebar-offcanvas" id="sidebar" role="navigation">
    <ul class="nav">
        <li class="active"><a href="ohss.htm">Home</a></li>
        <li><a href="ohss_setup.htm">OHSS Setup</a></li>
        <li><a href="ohss_workflow.htm">OHSS Workflow</a></li>
        <li><a href="ohss_ui_page_explanation.htm">UI Pages Explanation</a></li>
        <li><a href="#" data-toggle="collapse" data-target="#sub1">MiddleWare Explanation</a>
            <ul class="nav collapse" id="sub1">
                <li><a href="ohss_controller_explanation.htm">Controller Explanation</a></li>
                <li><a href="ohss_services_explanation.htm">Services Explanation</a></li>
                <li><a href="ohss_dao_explanation.htm">Dao Explanation</a></li>
            </ul>
        </li>
        <li><a href="ohss_database_explanation.htm">Database Explanation</a></li>
        <li><a href="ohss_faq.htm">FAQ</a></li>
    </ul>
</div>
