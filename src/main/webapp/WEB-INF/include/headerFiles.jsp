<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<head>
	<link rel="stylesheet" type="text/css" href="resources/css/build.css "/>
	<script src="resources/js/angular.min.js"></script>
    <script src="resources/js/jquery.combined.min.js"></script>
    <script src="resources/js/bootstrap.min.js"></script>

</head>
<body class="main-container">
  <nav class="navbar navbar-default">
	  <ul class="nav navbar-nav">
		<li class="logo"><a href="#">Knowledge Base</a></li>
		<li><a href="/spring-demo">Home</a></li>
		<li>
			<a href="#" class="dropdown-toggle" data-toggle="dropdown">Apps <span class="caret"></span></a>
			<ul class="dropdown-menu" role="menu">
				<li><a href="/spring-demo/ohss.htm">OHSS</a></li>
				<li><a href="/spring-demo/lhat.htm">LHAT</a></li>
				<li><a href="/spring-demo/sit.htm">SIT</a></li>
				<li><a href="/spring-demo/coreServices.htm">Core Services</a></li>
            </ul>
		</li>
        <li><a href="/spring-demo/angular.htm">Angular</a></li>
        <li><a href="/spring-demo/People.htm">Administration</a></li>
		<li><a href="#">About</a></li>
		<li><a href="#">Help</a></li>
	  </ul>


  </nav>
</body>

