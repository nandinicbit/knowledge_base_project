<%@include file="../../include/headerFiles.jsp" %>
<%@include file="../../include/sideBar.jsp" %>
<div class="main-container content-container">
    <h3>Welcome to OHSS(Occupational Health Surveillance System)</h3>
    <p>
        This system is designed to document and provide preventive medicine services and health-related education
        to those employees who have contact with live vertebrate animals or biohazardous material during the course
        of their employment at University of California.
    </p>

</div>