<%@include file="../../include/headerFiles.jsp" %>
<%@include file="../../include/sideBar.jsp" %>
<h3>UI Pages Explanation</h3>
<div class="col-xs-8">
<p>Before Learning more about UI pages, We must understand the concept of spoofing used across most of the applications.
    In the Development environment, we can login as other person(roles) so that we can test/develop all aspects of applications process.
    This concept of logging in as other person is called as Spoofing.

    <p>Roles : There are currently two roles assigned to the users explicitly :</p>
           <ul>
                <li>(a) Reviewer - Reviewer can see all the Risk Assessments and Statuses for the employees and can send email notifications</li>
                <li>(b) Admin/Medical- Medical person can create a medical assessment for the employees and can also view a health questionnaire(HQ),Risk Assessment(RA) and Medical Assessment(MA)</li>

           </ul>
</p>
<h5>UI Pages:</h5>
<ol class="list-group">
        <li class="list-group-item">
            <div><a data-toggle="collapse" href="#login_page">Login Page  (loginOhss.jsp)</a>
                <div class="collapse" id = "login_page">
                    <ul>
                        <li>If the local ohss instance is running, User can access OHSS at "localhost:8080/ohss"</li>
                        <li>From here User can click either on the spoof(if it is a dev environment)
                            or on sign in button if it is a production or staging environment and log into the system.</li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div><a data-toggle="collapse" href="#spoof_page">Spoofing Page  (spoof.jsp , Dev Environment ONLY)</a>
                <div class="collapse" id = "spoof_page">
                    <ul>
                        <li>From the login page, User can navigate to the spoofing page by clicking on the spoof page.</li>
                        <li>From this page user can spoof as any person from the list and login to the system.</li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div><a data-toggle="collapse" href="#landing_page">Landing Page  (landing.jsp)</a>
                <div class="collapse" id = "landing_page">
                    <ul>
                        <li>The landing page is the gateway to the OHSS application.</li>
                        <li>On this page user will see all the notifications regarding Risk Assessments(RA),Health Questionnaire(HQ),Medical Assessments(MA).</li>
                        <li>If the user is either a Reviewer or No role assigned, User can create a risk assessment from this page by clicking on the create risk assessment link</li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div><a data-toggle="collapse" href="#ra_form">Risk Assessment Form  (riskAssessmentForm.jsp)</a>
                <div class="collapse" id = "ra_form">
                    <ul>
                        <li>User can create a RA by clicking on the Create Risk Assessment link on the landing page.</li>
                        <li>Clicking on the link would navigate to the riskAssessmentForm.jsp</li>
                        <li>User can search for the employee and select the employee in order to create the Risk Assessment.</li>
                        <li>This page includes riskAssessmentBody.jsp which has all the questions.</li>
                        <li>After answering all the questions, user can save the forms by clicking on the create risk assessment form button.This will
                        cre</li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div><a data-toggle="collapse" href="#ra_form3">Risk Assessment Form3  (riskAssessmentForm3.jsp)</a>
                <div class="collapse" id = "ra_form3">
                    <ul>
                        <li>Once a user selects the employee to create a RA for, user is navigated to Risk Assessment Form3 page.</li>
                        <li>This page includes riskAssessmentBody.jsp which has all the questions.</li>
                        <li>After answering all the questions, user can save the forms by clicking on the 'Create Risk Assessment' button.This will
                            create a risk assessment form for the selected employee</li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div><a data-toggle="collapse" href="#ra_body">Risk Assessment Body (riskAssessmentBody.jsp)</a>
                <div class="collapse" id = "ra_body">
                    <ul>
                        <li>This page has all the questions and contents the RA form.</li>
                        <li>This page is included in riskAssessmentForm3.jsp,riskAssessmentCopy.jsp and riskAssessmentReview.jsp.</li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div><a data-toggle="collapse" href="#ra_form_conf">Risk Assessment Form Confirmation (riskAssessmentFormConf.jsp)</a>
                <div class="collapse" id = "ra_form_conf">
                    <ul>
                        <li>Once a RA form is created/updated, the control is navigated to risk assessment form confirmation page.</li>
                        <li>Supervisor can send email to the employee from this page(Sending email is optional.)</li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div><a data-toggle="collapse" href="#ra_form_edit">Risk Assessment Edit (riskAssessmentEdit.jsp)</a>
                <div class="collapse" id = "ra_form_edit">
                    <ul>
                        <li>Once a RA form is created, supervisor will still be able to edit the contents of the form until the employee agrees/disagrees on the RA form.</li>
                        <li>Supervisor will be redirected to the Risk Assessment Edit form when he clicks the Edit menu item from Incomplete Assessments Tab.</li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div><a data-toggle="collapse" href="#ra_inprogress">In Progress Assessments (inProgressAssessments.jsp)</a>
                <div class="collapse" id = "ra_inprogress">
                    <ul>
                        <li>One can navigate to InProgressAssessments page by clicking on the All Assessments tab</li>
                        <li>This page will have all the information regarding the assessments which are yet to be complete the entire process cycle of RA,HQ and MA.</li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="list-group-item">
            <div><a data-toggle="collapse" href="#ra_inprogress">In Progress Assessments (inProgressAssessments.jsp)</a>
                <div class="collapse" id = "ra_inprogress">
                    <ul>
                        <li>InProgress assessments</li>
                        <li>Supervisor will be redirected to the Risk Assessment Edit form when he clicks the Edit menu item from Incomplete Assessments Tab.</li>
                    </ul>
                </div>
            </div>
        </li>
</ol>
</div>

