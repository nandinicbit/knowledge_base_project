<%@include file="../../include/headerFiles.jsp" %>
<%@include file="../../include/sideBar.jsp" %>
<div id="faqs" style="padding-right: 60px;" class="center-content">
    <h3>Frequently Asked Questions</h3>
    <ul>
        <li>
            <div><a data-toggle="collapse" href="#ohss_1">How does OHSS work?</a>
                <div class="collapse help-answers" id = "ohss_1">
                    <img alt="email" src="../resources/images/ohss-process.png" width = "1000" style="margin-left:-105px">
                </div>
            </div>
        </li>
        <li>
            <div><a data-toggle="collapse" href="#ohss_2">How do I access OHSS?</a>
                <div class="collapse" id = "ohss_2">
                    <span class="help-answers"><p>In order to access the online application, you must log in using your campus credentials.</p></span>
                </div>
            </div>
        </li>
        <li>
            <div><a data-toggle="collapse" href="#ohss_3">What medical information do I need to have to access the application?</a>
                <div class="collapse" id = "ohss_3">
                    <span class="help-answers"><p>A copy of your medical history and immunizations dates are recommended</p></span>
                </div>
            </div>
        </li>
        <li>
            <div><a data-toggle="collapse" href="#ohss_4">Is my health Information being protected?</a>
                <div class="collapse" id = "ohss_4">
                    <span class="help-answers"><p>Yes. The flow of information through OHSS allows Personal Health Information to only be shared with the medical provider and the data is stored on secured servers.</p></span>
                </div>
            </div>
        </li>
        <li>
            <div><a data-toggle="collapse" href="#ohss_5">Will my health status be revealed to my supervisor?</a>
                <div class="collapse" id = "ohss_5">
							<span class="help-answers">
								<p>	No. A system firewall prevents supervisors or PIs from discovering any individual health information. The only information appropriate for the supervisor is whether or not an individual has been medically cleared to work on the project in question.</p>
								<p>In the rare event that additional safety concerns are identified on the questionnaire, the supervisor would only be told what steps they need to accomplish to permit the employee to safely work in the environment. However, personal information would NOT be shared with them.</p>
							</span>
                </div>
            </div>
        </li>
        <li>
            <div><a data-toggle="collapse" href="#ohss_6">Why is this information required?</a>
                <div class="collapse" id = "ohss_6">
                    <span class="help-answers"><p>Under Campus policy, everyone who works with animals, biological agents, or other hazardous agents or materials must be enrolled in the campus Occupational Health Surveillance program. This requirement protects employees and helps educate supervisors, principal investigators, and their employees about the health risks associated with work conducted in their facilities. The information also provides the Occupational Health physician an opportunity to counsel supervisors and employees on the best ways to minimize the health impacts of any of the risks, and to establish health care programs for employees at the highest risk.</p></span>
                </div>
            </div>
        </li>
        <li>
            <div><a data-toggle="collapse" href="#ohss_7">What can I expect once I complete the application process?</a>
                <div class="collapse" id = "ohss_7">
							<span class="help-answers"><p>Participants can expect any of the following:</p>
								<ul>
                                    <li>Approval to work for increments of 3 months, 6 months, or one year.</li>
                                    <li>An appointment referral for consultation and further assessment. The Participant must obtain authorization and a recharge account prior to making an appointment at Occupational Health Services.</li>
                                    <li>Immunizations, respirator fit test, or other services may be recommended based on risk and health history of the participant.</li>
                                </ul>
							</span>
                </div>
            </div>
        </li>
        <li>
            <div><a data-toggle="collapse" href="#ohss_8">What are the benefits of OHSS?</a>
                <div class="collapse" id = "ohss_8">
						<span class="help-answers"><ul>
                            <li>Minimizes the risk of improper handling Personal Identifiable Information</li>
                            <li>Ensures all mandatory information is captured in one system</li>
                            <li>Houses OHSS status information in one central location</li>
                            <li>Streamlines the process of being integrated into a protocol</li>
                        </ul></span>
                </div>
            </div>
        </li>
    </ul>
</div>

