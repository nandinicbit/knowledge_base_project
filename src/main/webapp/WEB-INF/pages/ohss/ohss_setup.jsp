<%@include file="../../include/headerFiles.jsp" %>
<%@include file="../../include/sideBar.jsp" %>
<h3>OHSS Setup</h3>
<div class="col-xs-8">
    <p>OHSS is developed using Spring Framework, Please find more details about spring framework <a href="http://en.wikipedia.org/wiki/Spring_Framework" target="_blank">here</a></p>
    <h5><b>Download Appropriate Applications:</b></h5>
    <div class="indent">
    <p>(1) The following steps need to be followed for getting your system ready for OHSS</p>
         <ul>
            <li>Download Java 7 <a href="http://www.oracle.com/technetwork/java/javase/downloads/jre7-downloads-1880261.html" target="_blank">here</a></li>
            <li>Download Apache Tomcat <a href="https://tomcat.apache.org/download-60.cgi" target="_blank">here</a></li>
            <li>Download Maven <a href="https://maven.apache.org/download.cgi?Preferred=ftp://mirror.reverse.net/pub/apache/" target="_blank">here</a></li>
            <li>Download an IDE (<a href="https://spring.io/tools/sts/all" target="_blank">Spring Tool Suite</a> , <a href="https://www.jetbrains.com/idea/download/" target="_blank">Intellij</a> etc) </li>
            <li>Download Sql Server(<a target="_blank" href="https://msdn.microsoft.com/en-us/sqlserver/bb671149.aspx">Sql Server</a>,<a href="http://www.microsoft.com/en-us/download/details.aspx?id=7593" target="_blank">Sql Server Management Studio</a>)</li>
            <li>Download a local instance of git bash command prompt(<a href="http://git-scm.com/download/win" target="_blank">Git Bash</a>)</li>
        </ul>
    <p>(2) Code base for most of the applications is bit bucket.Learn more about Bit Bucket <a href="http://en.wikipedia.org/wiki/Bitbucket" target="_blank">here</a></p>
        <ul>
            <li>Link to OHSS code base: <a href="https://bitbucket.org/ucd-itservices/ohss" target="_blank">OHSS</a> (If you cannot access code base please talk to your team lead.)</li>
            <li>OHSS uses Less to compile css files.Learn more about less <a href="http://en.wikipedia.org/wiki/Less_%28stylesheet_language%29" target="_blank">here</a></li>
        </ul>
    </div>
    <h5><b>Setting up Environment:</b></h5>
    <div class="indent">
        <p>(1) OHSS Project
               <ul>
                    <li>Open git bash in git folder on your local system</li>
                    <li>Type SSH Keygen and copy the public key</li>
                    <li>In Bitbucket, Open 'Manage Account' from right hand top corner dropdown</li>
                    <li>Open SSH Keys under Security and paste the public key</li>
                    <li>In git bash, type "git clone git@bitbucket.org:ucd-itservices/ohss.git". This will create a git project</li>
                </ul>
        </p>

        <p>(2) IDE</p>
        <ul>
            <li>Click on File,import project and select the ohss folder created from above, Follow next steps and import the project.</li>
            <li>On the Debug configuration, Create a maven runtime configuration with command line as "tomcat7:run" </li>
            <li>Under before launch section, add "Make Project and clean:clean as options"</li>
            <li>Set Maven and Java home appropriately(Please create Java_home and Maven_home under <a href="http://www.robertsindall.co.uk/blog/setting-java-home-variable-in-windows/" target="_blank">system settings</a> as well.)</li>
        </ul>
        <p>(3) SQl Server</p>
        <ul>

        </ul>
    </div>

</div>