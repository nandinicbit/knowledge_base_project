<%@include file="../include/headerFiles.jsp" %>
<div class="main-container content-container">
    <form action = "addPeople" method = "POST" class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-sm-2">FirstName:</label>
            <div class="col-sm-10">
                <input  type = "text" name = "firstName" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Last Name:</label>
            <div class="col-sm-10">
                <input  type = "text" name = "lastName" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">Address:</label>
            <div class="col-sm-10">
                <input  type = "text" name = "address" />
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2">City:</label>
            <div class="col-sm-10">
                <input  type = "text" name = "city" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>

    </form>
</div>